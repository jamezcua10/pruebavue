import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Dashboard from '../views/Dashboard.vue'
import Editar from '../views/Editar.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '*',
    redirect: '/'
  },
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard,
    meta:{
      authentication: true
    }
  },
  {
    path: '/editar/:id',
    name: 'Editar',
    component: Editar,
    meta:{
      authentication: true
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  let token = localStorage.getItem('token');
  let authentication = to.matched.some(record => record.meta.authentication);
  if(authentication && !token){
    next('')
  }else{
    next();
  }
})

export default router